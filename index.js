const Hapi = require('hapi');
const led = require('./led');
const server = new Hapi.Server({
    port: 3000
});


server.route({
    method: 'GET',
    path: '/',
    handler: function (req, h) {
        return h.view('main', {
            state1: intToString(led.LEDStatus[0]),
            state2: intToString(led.LEDStatus[1])
        });

    }
});

server.route({
    method: 'GET',
    path: '/{filename}',
    handler: function (req, h) {

        return h.file(req.params.filename);
    }
});

server.route({
    method: 'POST',
    path: '/led/{id}',
    handler: function (req, h) {
        var err = false;
        var index = req.payload.id-1;
        var state = req.payload.state;
        if (state == "on")
            led.LEDStatus[index] = 1;
        else if (state == "off")
            led.LEDStatus[index] = 0;
        else
            err = true;

        if (err)
        {
            console.log("No payload");
            return {
                err: err
            };
        }
        else {
            //led.LedStateChange(req.params.id,led_status);
            console.log(req.params.id + " "+req.payload.state);
            return {
                state: intToString(led.LEDStatus[index])
            };
        }
    }
});

async function startServer() {
    await server.register([require('vision'),require('inert')]);
    await server.start();

    server.views({
        engines: {
            html: require('handlebars')
        },
        relativeTo: __dirname,
        path: 'templates'
    });

    console.log('Server on ' + server.info.port);
}

function intToString(v) {
    if (v == 0)
        return "off";
    else
        return "on";
}

startServer();

module.exports = server;